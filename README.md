## Convert YouTube playlist to AmplitudeJS based static web content.

First check and edit config.conf file for you.

run `all_in_one_run.sh` script to archiving:

```bash
./all_in_one_run.sh
```

then complete steps.

If you are really sure of what you are doing, you can give yes prompt so that you do not wait:

```bash
yes | ./all_in_one_run.sh
```

Live example: 
[kurdishrock.gitlab.io](https://kurdishrock.gitlab.io)
