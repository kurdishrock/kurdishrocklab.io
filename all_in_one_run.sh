#!/bin/bash
. generate.sh true &>/dev/null #import functions and configs

function check_command() { [ -x "$(command -v $1)" ] || { echo -e "\e[31m$1 not found, please install it.\e[0m" ; } }
check_command youtube-dl
check_command ffmpeg
check_command convert
check_command exiftool

function confirm() {
  local _prompt _default _response
  if [ "$1" ]; then _prompt="$1"; else _prompt="Are you sure"; fi
  _prompt="$_prompt [y/n] ?"
  while true; do
    read -r -p "$_prompt " _response
    case "$_response" in
      [Yy][Ee][Ss]|[Yy])
        return 0
        ;;
      [Nn][Oo]|[Nn])
        return 1
        ;;
      *)
        ;;
    esac
  done
}

echo -e "\e[32mcheck config.conf file before run\e[0m"
confirm || exit 1

echo -e "\e[32myoutube playlist \e[33m${youtube_playlist_url}\e[0m \e[32mwill be downloaded\e[0m"
confirm && playlist

echo -e "\e[32mfile name of songs will be renamed as clean url\e[0m"
confirm && slug

echo -e "\e[32mthumb image of songs will be extracted\e[0m"
confirm && cover

echo -e "\e[32metherpad sources will be downloaded\e[0m"
confirm && etherpad

echo -e "\e[32mdownload links will be created\e[0m"
confirm && download

echo -e "\e[32msongs.js and songs.json will be created\e[0m"
confirm && ./songs.js.sh

echo -e "\e[32mindex.html will be created\e[0m"
confirm && ./index.html.sh
