#!/bin/bash
. generate.sh true &>/dev/null #import functions and configs

cat > public/index.html <<EOF
<!DOCTYPE html>
<html>
	<head>
              	<meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>${site_title}</title>

		<!-- Include Amplitude JS -->
		<script type="text/javascript" src="${AmplitudeJS}"></script>

		<!-- Include Style Sheet -->
		<link rel="stylesheet" type="text/css" href="css/app.css"/>

	</head>
	<body>
		<!-- Blue Playlist Container -->
		<div id="blue-playlist-container">

			<!-- Amplitude Player -->
			<div id="amplitude-player">

				<!-- Left Side Player -->
				<div id="amplitude-left">
					<img data-amplitude-song-info="cover_art_url"/>
					<div id="player-left-bottom">
						<div id="time-container">
							<span class="current-time">
								<span class="amplitude-current-minutes" ></span>:<span class="amplitude-current-seconds"></span>
							</span>
							<div id="progress-container">
								<input type="range" class="amplitude-song-slider"/>
								<progress id="song-played-progress" class="amplitude-song-played-progress"></progress>
								<progress id="song-buffered-progress" class="amplitude-buffered-progress" value="0"></progress>
							</div>
							<span class="duration">
								<span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
							</span>
						</div>

						<div id="control-container">
							<div id="repeat-container">
								<div class="amplitude-repeat" id="repeat"></div>
								<div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle"></div>
							</div>

							<div id="central-control-container">
								<div id="central-controls">
									<div class="amplitude-prev" id="previous"></div>
									<div class="amplitude-play-pause" id="play-pause"></div>
									<div class="amplitude-next" id="next"></div>
								</div>
							</div>

							<div id="volume-container">
								<div class="volume-controls">
									<div class="amplitude-mute amplitude-not-muted"></div>
									<input type="range" class="amplitude-volume-slider"/>
									<div class="ms-range-fix"></div>
								</div>
								<div class="amplitude-shuffle amplitude-shuffle-off" id="shuffle-right"></div>
							</div>
						</div>

						<div id="meta-container">
							<span data-amplitude-song-info="name" class="song-name"></span>

							<div class="song-artist-album">
								<span data-amplitude-song-info="artist"></span>
								<span data-amplitude-song-info="album"></span>
							</div>
						</div>
					</div>
				</div>
				<!-- End Left Side Player -->

				<!-- Right Side Player -->
				<div id="amplitude-right">

				$(html)

				</div>
				<!-- End Right Side Player -->
			</div>
			<!-- End Amplitdue Player -->
		</div>

		<div class="center">
			<p>
				<img alt="total views" src="https://profile-counter.glitch.me/${visitor_count}/count.svg"><br>
				GitLab Source: <a href="${gitlab_source}" target="_blank" style="color:aqua;text-decoration:none">${gitlab_show}</a></br>
				Suggest song on <a href="${google_forms_url}" target="_blank" style="color:aqua;text-decoration:none">Google Forms</a><br>
				Telegram Channel: <a href="${telegram}" target="_blank" style="color:aqua;text-decoration:none">${telegram_short}</a><br>
				Synced from <a href="${youtube_playlist_url}" target="_blank" style="color:aqua;text-decoration:none">YouTube Playlist</a> by <a href="data/command.txt" target="_blank" style="color:aqua;text-decoration:none">youtube-dl</a><br>
				${COPYRIGHT}
				<a href="data/songs.json" target="_blank" style="color:white;">JSON DATA</a> |
                                <a href="data/etherpad.html" target="_blank" style="color:white;">Etherpad HTML</a> |
                                <a href="data/etherpad.etherpad" target="_blank" style="color:white;">Etherpad File</a> |
                                <a href="data/download.html" target="_blank" style="color:white;">Download All Songs</a>
			</p>
			<br>
		</div>


		<!--
			Include UX functions JS

			NOTE: These are for handling things outside of the scope of AmplitudeJS
		-->
		<script type="text/javascript" src="js/functions.js"></script>
                <script type="text/javascript" src="js/songs.js"></script>

	</body>
</html>

EOF
