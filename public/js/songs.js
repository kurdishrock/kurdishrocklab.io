Amplitude.init(
{
        "songs": [
		          {
                  "name": "Again",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/again.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/again.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=wyLOpLpGeic"
          },
          {
                  "name": "A Separation",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/a-separation.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/a-separation.mp3",
                  "album": "Hespên Bajêr",
                  "youtube_url": "https://www.youtube.com/watch?v=QRbFnfNM0sM"
          },
          {
                  "name": "Asiyen Tebaxe",
                  "artist": "Koma Rewşen, Harun Ataman, Harun Ataman",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/asiyen-tebaxe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/asiyen-tebaxe.mp3",
                  "album": "Sitranên Dilê Me, Vol. 6",
                  "youtube_url": "https://www.youtube.com/watch?v=Cts_4K6D1cg"
          },
          {
                  "name": "Ax Lê Kinê",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ax-le-kine.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ax-le-kine.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=McVaLGha7lU"
          },
          {
                  "name": "Ax Û Eman",
                  "artist": "Ciwan Haco, Ciwan Haco, Dilber Haco, Nico Sieveking, Ayhan Evci, Ayhan Evci",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ax-u-eman.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ax-u-eman.mp3",
                  "album": "Na Na",
                  "youtube_url": "https://www.youtube.com/watch?v=hriUP0f19KE"
          },
          {
                  "name": "Bakûr",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bakur.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bakur.mp3",
                  "album": "Hespên Bajêr",
                  "youtube_url": "https://www.youtube.com/watch?v=NWibc-JDIdc"
          },
          {
                  "name": "Balada Şivanekî (Bijareyên Kovara Ziryabê)",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/balada-sivaneki-bijareyen-kovara-ziryabe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/balada-sivaneki-bijareyen-kovara-ziryabe.mp3",
                  "album": "Olana Bajêr (Bijareyên Kovara Ziryabê): Ziryab Magazine Selections",
                  "youtube_url": "https://www.youtube.com/watch?v=34tCalG7PGc"
          },
          {
                  "name": "Baranek",
                  "artist": "Mirady",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/baranek.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/baranek.mp3",
                  "album": "Xemname Gamname",
                  "youtube_url": "https://www.youtube.com/watch?v=3FOjvk5HHkM"
          },
          {
                  "name": "Bayê Payîzê",
                  "artist": "Harûn, Kerem Gerdenzerî, Mikaîlê Reşîd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/baye-payize.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/baye-payize.mp3",
                  "album": "Rev",
                  "youtube_url": "https://www.youtube.com/watch?v=Kfkld1-kzps"
          },
          {
                  "name": "Bazdam Bazdam",
                  "artist": "Mehmet Atlı, Mehmet Atlı, Mehmet Atlı",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bazdam-bazdam.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bazdam-bazdam.mp3",
                  "album": "Jahr",
                  "youtube_url": "https://www.youtube.com/watch?v=C8ho8AcmMnU"
          },
          {
                  "name": "Bêje",
                  "artist": "Sîdar",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/beje.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/beje.mp3",
                  "album": "Bêje",
                  "youtube_url": "https://www.youtube.com/watch?v=S9NilG1RAo8"
          },
          {
                  "name": "Belengaz",
                  "artist": "Sîdar",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/belengaz.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/belengaz.mp3",
                  "album": "Bêje",
                  "youtube_url": "https://www.youtube.com/watch?v=iyXF7urKCn4"
          },
          {
                  "name": "Berî hun min daliqînin",
                  "artist": "Qeydubend",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/beri-hun-min-daliqinin.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/beri-hun-min-daliqinin.mp3",
                  "album": "NOSTALGIA 1997",
                  "youtube_url": "https://www.youtube.com/watch?v=oOadTPGH2DQ"
          },
          {
                  "name": "Bernadim",
                  "artist": "Harûn, Harûn Ataman, Harûn Ataman",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bernadim.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bernadim.mp3",
                  "album": "Rev",
                  "youtube_url": "https://www.youtube.com/watch?v=iMFApNgEPek"
          },
          {
                  "name": "Bi Kijan Meqami (Bijareyên Kovara Ziryabê)",
                  "artist": "Teşqele",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bi-kijan-meqami-bijareyen-kovara-ziryabe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/bi-kijan-meqami-bijareyen-kovara-ziryabe.mp3",
                  "album": "Olana Bajêr (Bijareyên Kovara Ziryabê): Ziryab Magazine Selections",
                  "youtube_url": "https://www.youtube.com/watch?v=zPMdsKqFZR0"
          },
          {
                  "name": "Birîna Reş",
                  "artist": "Sîmir Rûdan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/birina-res.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/birina-res.mp3",
                  "album": "Bi çîrokî",
                  "youtube_url": "https://www.youtube.com/watch?v=zUyYUYlii6M"
          },
          {
                  "name": "Brîndar",
                  "artist": "Koma Wetan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/brindar.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/brindar.mp3",
                  "album": "Kurdish Rock",
                  "youtube_url": "https://www.youtube.com/watch?v=AMYu7MMVjow"
          },
          {
                  "name": "Carek Din",
                  "artist": "Mesut Çetinkaya",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/carek-din.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/carek-din.mp3",
                  "album": "Kêm",
                  "youtube_url": "https://www.youtube.com/watch?v=kLf8z7ZytEQ"
          },
          {
                  "name": "Çar Zarok (Dört Çocuk)",
                  "artist": "Teq Û Req",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/car-zarok-dort-cocuk.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/car-zarok-dort-cocuk.mp3",
                  "album": "Keft Û Left (Didinme)",
                  "youtube_url": "https://www.youtube.com/watch?v=GGeR7ToPO0w"
          },
          {
                  "name": "Dagirker",
                  "artist": "Ala Sor",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dagirker.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dagirker.mp3",
                  "album": "Dagirker",
                  "youtube_url": "https://www.youtube.com/watch?v=k22wMZvTGPI"
          },
          {
                  "name": "Dera Sorê",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dera-sore.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dera-sore.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=S2Xsi_LJvfo"
          },
          {
                  "name": "Dew Dew",
                  "artist": "Aynur Doğan, Demir Demirkan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dew-dew.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dew-dew.mp3",
                  "album": "Biriz",
                  "youtube_url": "https://www.youtube.com/watch?v=FlV9s6JHyDE"
          },
          {
                  "name": "Dil Ketime (Tutuldum)",
                  "artist": "Ciwan Haco, Ciwan Haco, Ciwan Haco, Ayhan Evci",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dil-ketime-tutuldum.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dil-ketime-tutuldum.mp3",
                  "album": "Off",
                  "youtube_url": "https://www.youtube.com/watch?v=Erd1YFdFqDY"
          },
          {
                  "name": "Dilo",
                  "artist": "Sîdar",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dilo.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/dilo.mp3",
                  "album": "Bêje",
                  "youtube_url": "https://www.youtube.com/watch?v=s0pVdZbOUYk"
          },
          {
                  "name": "Dîroka Cureyekî",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/diroka-cureyeki.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/diroka-cureyeki.mp3",
                  "album": "Hespên Bajêr",
                  "youtube_url": "https://www.youtube.com/watch?v=Y9fVfiVzPMw"
          },
          {
                  "name": "Egit",
                  "artist": "Rotinda, Rotînda Yetkîner, Rotînda Yetkîner",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/egit.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/egit.mp3",
                  "album": "Koîro",
                  "youtube_url": "https://www.youtube.com/watch?v=aIF5bRCoKRY"
          },
          {
                  "name": "Elegez",
                  "artist": "Rewşen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/elegez.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/elegez.mp3",
                  "album": "Pola Nû",
                  "youtube_url": "https://www.youtube.com/watch?v=yYiUgQI4SqA"
          },
          {
                  "name": "Erzan",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/erzan.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/erzan.mp3",
                  "album": "Erzan",
                  "youtube_url": "https://www.youtube.com/watch?v=d7k2Jl_jDuc"
          },
          {
                  "name": "Evdilcebar",
                  "artist": "Yunus Dişkaya, Yunus Diṣkaya, Rojen Barnas, Yunus Diṣkaya",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/evdilcebar.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/evdilcebar.mp3",
                  "album": "Sola Sebrê",
                  "youtube_url": "https://www.youtube.com/watch?v=ac3S5n_Dttk"
          },
          {
                  "name": "Fate",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/fate.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/fate.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=VvaolD7YS2E"
          },
          {
                  "name": "Ferec - Bêje (Official Video)",
                  "artist": "Ferec Official",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-beje-official-video.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-beje-official-video.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=aetgeE6R7Ok"
          },
          {
                  "name": "Çine",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-cine.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-cine.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=zXRsZozmORU"
          },
          {
                  "name": "Maf",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-maf.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-maf.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=R-zVj2siPC8"
          },
          {
                  "name": "Na Na",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-na-na.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/ferec-na-na.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=owesPF4uEcA"
          },
          {
                  "name": "Gimgim",
                  "artist": "Rotinda, Rotînda, Rotînda",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gimgim.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gimgim.mp3",
                  "album": "Naygotin",
                  "youtube_url": "https://www.youtube.com/watch?v=-jnM_DImolE"
          },
          {
                  "name": "Gula Sor",
                  "artist": "Ciwan Haco",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gula-sor.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gula-sor.mp3",
                  "album": "Gula Sor (Remastered)",
                  "youtube_url": "https://www.youtube.com/watch?v=X0M8MgoEi8Y"
          },
          {
                  "name": "Gulevin",
                  "artist": "Jan Axin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gulevin.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/gulevin.mp3",
                  "album": "Evînên Şikestî",
                  "youtube_url": "https://www.youtube.com/watch?v=7Xck4nBNhUY"
          },
          {
                  "name": "Hatin",
                  "artist": "Rewşen, Arame Dikran, Simoye Şemo",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hatin.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hatin.mp3",
                  "album": "Evina Aram",
                  "youtube_url": "https://www.youtube.com/watch?v=PcANM0jtn_w"
          },
          {
                  "name": "Havin (Yaz)",
                  "artist": "Ciwan Haco, Ciwan Haco, Ciwan Haco, Ayhan Evci",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/havin-yaz.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/havin-yaz.mp3",
                  "album": "Off",
                  "youtube_url": "https://www.youtube.com/watch?v=rudYDAe19PE"
          },
          {
                  "name": "Havin / Yaz / The Summer",
                  "artist": "Diljen Ronî, Diljen Ronî, Diljen Ronî",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/havin-yaz-the-summer.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/havin-yaz-the-summer.mp3",
                  "album": "Du Demsal",
                  "youtube_url": "https://www.youtube.com/watch?v=3HWXbysv_M0"
          },
          {
                  "name": "Hejaro",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hejaro.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hejaro.mp3",
                  "album": "Hejaro",
                  "youtube_url": "https://www.youtube.com/watch?v=MLKatzCR9Bo"
          },
          {
                  "name": "Helîkoptêr",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/helikopter.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/helikopter.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=8YJeey8b4ig"
          },
          {
                  "name": "Heq! Heq!",
                  "artist": "Bajar",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/heq-heq.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/heq-heq.mp3",
                  "album": "Yaklaş",
                  "youtube_url": "https://www.youtube.com/watch?v=_BIJGfdkpM0"
          },
          {
                  "name": "Hevi",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hevi.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/hevi.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=0h982o5HXAo"
          },
          {
                  "name": "I Want to Be Like That",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/i-want-to-be-like-that.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/i-want-to-be-like-that.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=JklmNfS4wc0"
          },
          {
                  "name": "I Will Not Go",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/i-will-not-go.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/i-will-not-go.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=qn5jK75CdTc"
          },
          {
                  "name": "Jana Dilê Min",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/jana-dile-min.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/jana-dile-min.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=l1OccQTkGvE"
          },
          {
                  "name": "Jan Axin - Evindar",
                  "artist": "Ares Müzik",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/jan-axin-evindar.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/jan-axin-evindar.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=2ttBnGjTTtg"
          },
          {
                  "name": "Keça Peri (2)",
                  "artist": "Kerem Gerdenzeri",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/keca-peri-2.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/keca-peri-2.mp3",
                  "album": "Peşk",
                  "youtube_url": "https://www.youtube.com/watch?v=0JiMhkrDe5k"
          },
          {
                  "name": "Keçık Çıma Dıtırsî",
                  "artist": "Kerem Gerdenzeri",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kecik-cima-ditirsi.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kecik-cima-ditirsi.mp3",
                  "album": "Rojbaş Amed / Veger (Koma Wetan)",
                  "youtube_url": "https://www.youtube.com/watch?v=WTeXLDzhdCc"
          },
          {
                  "name": "Kilama Çivîka Zivistanê, Pt. 3",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kilama-civika-zivistane-pt-3.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kilama-civika-zivistane-pt-3.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=3eYNpJ8HNvg"
          },
          {
                  "name": "Koma Rewşen - Cane Di Beruke De(Êdî Bes)",
                  "artist": "Rock&Metal-Kurdî",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-cane-di-beruke-de-edi-bes.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-cane-di-beruke-de-edi-bes.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=vcFuNWJeDzs"
          },
          {
                  "name": "Qureye Sedsale",
                  "artist": "Rewşen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-qureye-sedsale-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-qureye-sedsale-official-audio.mp3",
                  "album": "Pola Nû",
                  "youtube_url": "https://www.youtube.com/watch?v=stBN75pDwAY"
          },
          {
                  "name": "Zagros",
                  "artist": "Rewşen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-zagros-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-rewsen-zagros-official-audio.mp3",
                  "album": "Pola Nû",
                  "youtube_url": "https://www.youtube.com/watch?v=JdF9dAp1hk4"
          },
          {
                  "name": "Koma Siyabend - Agît (Official Audio © Art Records)",
                  "artist": "ART RECORDS",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-siyabend-agit-official-audio-c-art-records.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-siyabend-agit-official-audio-c-art-records.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=MuBAiw1RBlw"
          },
          {
                  "name": "Pêrîşan",
                  "artist": "Koma Wetan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-perisan.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-perisan.mp3",
                  "album": "Kurdish Rock",
                  "youtube_url": "https://www.youtube.com/watch?v=eepL6LeISyU"
          },
          {
                  "name": "Şena Min",
                  "artist": "Koma Wetan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-sena-min.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-sena-min.mp3",
                  "album": "Kurdish Rock",
                  "youtube_url": "https://www.youtube.com/watch?v=AqgMUnGC9I8"
          },
          {
                  "name": "Kurdistana Min",
                  "artist": "Kerem Gerdenzeri",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-welate-me.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/koma-wetan-welate-me.mp3",
                  "album": "Rojbaş Amed / Veger (Koma Wetan)",
                  "youtube_url": "https://www.youtube.com/watch?v=AR7Vv3G3RTg"
          },
          {
                  "name": "Kûçık Hatın",
                  "artist": "Ercan Bingöl",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kucik-hatin.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kucik-hatin.mp3",
                  "album": "Ron",
                  "youtube_url": "https://www.youtube.com/watch?v=eW8IFKTCDjA"
          },
          {
                  "name": "kudish rock BeSnor",
                  "artist": "Golmaz",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kudish-rock-besnor.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kudish-rock-besnor.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=Pq6D4r0M1fk"
          },
          {
                  "name": "Kurdika",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kurdika.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/kurdika.mp3",
                  "album": "Kurdika",
                  "youtube_url": "https://www.youtube.com/watch?v=uvEYd6iH9sg"
          },
          {
                  "name": "Lawo",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/lawo.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/lawo.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=Tzw6EHggKuA"
          },
          {
                  "name": "Li Ber Berê",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/li-ber-bere.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/li-ber-bere.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=MFhBMJ-D-9M"
          },
          {
                  "name": "Li Ber Derî",
                  "artist": "Xeyal",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/li-ber-deri.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/li-ber-deri.mp3",
                  "album": "Li Ber Derî",
                  "youtube_url": "https://www.youtube.com/watch?v=iDXNnAaoB0g"
          },
          {
                  "name": "Lûrî Lûrî",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/luri-luri.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/luri-luri.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=T3cCzKXnzz8"
          },
          {
                  "name": "Mame Tî",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mame-ti.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mame-ti.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=120Dy56P86s"
          },
          {
                  "name": "Mehmet Yoldaş - Malan Barkır  ( Kürtçe Rock )",
                  "artist": "Mehmet Yoldaş",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mehmet-yoldas-malan-barkir-kurtce-rock.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mehmet-yoldas-malan-barkir-kurtce-rock.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=_8dtiRYmQ3I"
          },
          {
                  "name": "Mesele Ne Eva",
                  "artist": "Abbas Ahmed",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mesele-ne-eva.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/mesele-ne-eva.mp3",
                  "album": "Deynek",
                  "youtube_url": "https://www.youtube.com/watch?v=6PXj55eJ_iU"
          },
          {
                  "name": "Min Çi Dît",
                  "artist": "Koçer",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-ci-dit.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-ci-dit.mp3",
                  "album": "Min Çi Dît",
                  "youtube_url": "https://www.youtube.com/watch?v=J7l2oeq66R0"
          },
          {
                  "name": "Min Dirêj Bûye Şeva Min, Pt. 1",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-direj-buye-seva-min-pt-1.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-direj-buye-seva-min-pt-1.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=8QmmrASVXIo"
          },
          {
                  "name": "Min Dirêj Bûye Şeva Min, Pt. 2",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-direj-buye-seva-min-pt-2.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/min-direj-buye-seva-min-pt-2.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=fiC8eDKfgTk"
          },
          {
                  "name": "Na Na",
                  "artist": "Sîdar",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/na-na.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/na-na.mp3",
                  "album": "Bêje",
                  "youtube_url": "https://www.youtube.com/watch?v=91AQvCXbbUk"
          },
          {
                  "name": "Nazikê (Remastered)",
                  "artist": "Ciwan Haco",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/nazike-remastered.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/nazike-remastered.mp3",
                  "album": "Serhildan (Remastered)",
                  "youtube_url": "https://www.youtube.com/watch?v=QCzN8-8HRbU"
          },
          {
                  "name": "Nikarim",
                  "artist": "Xêro Abbas, Hekim Sefkan, Hekim Sefkan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/nikarim.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/nikarim.mp3",
                  "album": "Axîn",
                  "youtube_url": "https://www.youtube.com/watch?v=cljntqLLvR4"
          },
          {
                  "name": "Old Memories",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/old-memories.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/old-memories.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=VTq6eBZH94g"
          },
          {
                  "name": "Reading Zindanı Baladı",
                  "artist": "Jan Axin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/reading-zindani-baladi.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/reading-zindani-baladi.mp3",
                  "album": "Evînên Şikestî",
                  "youtube_url": "https://www.youtube.com/watch?v=WeiPs0AT64I"
          },
          {
                  "name": "Rê",
                  "artist": "Jan Axin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/re.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/re.mp3",
                  "album": "Zerîba",
                  "youtube_url": "https://www.youtube.com/watch?v=E0Pms88I-J0"
          },
          {
                  "name": "Reş Weba",
                  "artist": "Harûn, Harûn Ataman, Harûn Ataman",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/res-weba.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/res-weba.mp3",
                  "album": "Rev",
                  "youtube_url": "https://www.youtube.com/watch?v=wUQuYiq9-GQ"
          },
          {
                  "name": "Revîn",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/revin.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/revin.mp3",
                  "album": "Mayîndeyên Sînora",
                  "youtube_url": "https://www.youtube.com/watch?v=2BfF0KIP0sM"
          },
          {
                  "name": "Rev",
                  "artist": "Harûn, Harûn Ataman, Harûn Ataman",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/rev.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/rev.mp3",
                  "album": "Rev",
                  "youtube_url": "https://www.youtube.com/watch?v=LeziLKWVIHg"
          },
          {
                  "name": "Revoko",
                  "artist": "Rewşen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/revoko.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/revoko.mp3",
                  "album": "Pola Nû",
                  "youtube_url": "https://www.youtube.com/watch?v=scZlVwqcUX4"
          },
          {
                  "name": "Deri Vekin",
                  "artist": "Robin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-deri-vekin-saristanbul.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-deri-vekin-saristanbul.mp3",
                  "album": "Şaristanbul",
                  "youtube_url": "https://www.youtube.com/watch?v=v2vjet2YuT0"
          },
          {
                  "name": "Malxırabo",
                  "artist": "Robin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-malxirabo-saristanbul.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-malxirabo-saristanbul.mp3",
                  "album": "Şaristanbul",
                  "youtube_url": "https://www.youtube.com/watch?v=kl2Qt1drbKI"
          },
          {
                  "name": "Ti",
                  "artist": "Robin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-ti-saristanbul.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/robin-ti-saristanbul.mp3",
                  "album": "Şaristanbul",
                  "youtube_url": "https://www.youtube.com/watch?v=mbzJXsWcZF0"
          },
          {
                  "name": "Rojava",
                  "artist": "Nickwan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/rojava.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/rojava.mp3",
                  "album": "Rojava",
                  "youtube_url": "https://www.youtube.com/watch?v=2rrBC2ODC6g"
          },
          {
                  "name": "Sazê Min Tu Bistrê",
                  "artist": "Yunus Dişkaya, Yunus Dişkaya, Şikoyê Hesen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/saze-min-tu-bistre.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/saze-min-tu-bistre.mp3",
                  "album": "Sazê Min Tu Bistrê",
                  "youtube_url": "https://www.youtube.com/watch?v=yXfUO5PtNi8"
          },
          {
                  "name": "Sebra Azad",
                  "artist": "Jan Axin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sebra-azad.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sebra-azad.mp3",
                  "album": "Evînên Şikestî",
                  "youtube_url": "https://www.youtube.com/watch?v=VNg8LPm4-_w"
          },
          {
                  "name": "Sedo",
                  "artist": "Vedat Tanış",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sedo.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sedo.mp3",
                  "album": "Sedo",
                  "youtube_url": "https://www.youtube.com/watch?v=BV81TEBOEs4"
          },
          {
                  "name": "Şêrina Min",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/serina-min.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/serina-min.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=8AQ_vBUKkdU"
          },
          {
                  "name": "Şewêkî Bê Deng",
                  "artist": "Nickwan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/seweki-be-deng.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/seweki-be-deng.mp3",
                  "album": "Rohî Mirdû",
                  "youtube_url": "https://www.youtube.com/watch?v=1t5Vo2JdWU8"
          },
          {
                  "name": "Sînê",
                  "artist": "Koma Wetan",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sine.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/sine.mp3",
                  "album": "Kurdish Rock",
                  "youtube_url": "https://www.youtube.com/watch?v=m4QlAQWrvNE"
          },
          {
                  "name": "SIYA ŞEVÊ - Bihar",
                  "artist": "SIYA ŞEVÊ",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-bihar.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-bihar.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=guTfIihBTAc"
          },
          {
                  "name": "Siya Şevê - Derewe",
                  "artist": "SIYA ŞEVÊ",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-derewe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-derewe.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=YQFJO-Zk2Fs"
          },
          {
                  "name": "SIYA ŞEVÊ / Na Nebêje",
                  "artist": "SIYA ŞEVÊ",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-na-nebeje.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/siya-seve-na-nebeje.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=cJQay8KXG7s"
          },
          {
                  "name": "S.O.S",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/s-o-s.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/s-o-s.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=_O5CDhnMQQU"
          },
          {
                  "name": "Strange Occurrence",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/strange-occurrence.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/strange-occurrence.mp3",
                  "album": "Strange Occurrence",
                  "youtube_url": "https://www.youtube.com/watch?v=4Nk7Adq7XiY"
          },
          {
                  "name": "Beri (Özlem)",
                  "artist": "Teq Û Req",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/teq-u-req-beri-official-audio-c-kom-muzik.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/teq-u-req-beri-official-audio-c-kom-muzik.mp3",
                  "album": "Keft Û Left (Didinme)",
                  "youtube_url": "https://www.youtube.com/watch?v=GU5kQe_Hjxw"
          },
          {
                  "name": "The Dark Year",
                  "artist": "Rockurd",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/the-dark-year.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/the-dark-year.mp3",
                  "album": "Lunatic",
                  "youtube_url": "https://www.youtube.com/watch?v=xutFah4N0HY"
          },
          {
                  "name": "Walem Ke",
                  "artist": "Masoud Mohammadi",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/walem-ke.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/walem-ke.mp3",
                  "album": "Kamoter - Dove",
                  "youtube_url": "https://www.youtube.com/watch?v=U_PPD4CFtis"
          },
          {
                  "name": "Wax Wax",
                  "artist": "Jan Axin",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/wax-wax.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/wax-wax.mp3",
                  "album": "Evînên Şikestî",
                  "youtube_url": "https://www.youtube.com/watch?v=MCpb1zdaz6U"
          },
          {
                  "name": "Welatê Min",
                  "artist": "Rojhan Beken",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/welate-min.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/welate-min.mp3",
                  "album": "Lawo",
                  "youtube_url": "https://www.youtube.com/watch?v=1mqP5U9dme0"
          },
          {
                  "name": "Wey Gidi",
                  "artist": "Mehmet Atlı, Mehmet Atlı, Aryen Ari",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/wey-gidi.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/wey-gidi.mp3",
                  "album": "Jahr",
                  "youtube_url": "https://www.youtube.com/watch?v=YJaiGkFeZPI"
          },
          {
                  "name": "Xatun",
                  "artist": "Ferec",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xatun.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xatun.mp3",
                  "album": "Helîkoptêr",
                  "youtube_url": "https://www.youtube.com/watch?v=SNLJYUj13vM"
          },
          {
                  "name": "Xelîn (Bijareyên Kovara Ziryabê)",
                  "artist": "Ferît Sevîm",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xelin-bijareyen-kovara-ziryabe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xelin-bijareyen-kovara-ziryabe.mp3",
                  "album": "Olana Bajêr (Bijareyên Kovara Ziryabê): Ziryab Magazine Selections",
                  "youtube_url": "https://www.youtube.com/watch?v=bzU3ke3qYoc"
          },
          {
                  "name": "Xengilok",
                  "artist": "Harûn, Harûn Ataman, Rojhilat Azad",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xengilok.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xengilok.mp3",
                  "album": "Rev",
                  "youtube_url": "https://www.youtube.com/watch?v=_DF2kyGPsLw"
          },
          {
                  "name": "Xewnereşk (Kabus)",
                  "artist": "Teq Û Req",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xewneresk-kabus.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xewneresk-kabus.mp3",
                  "album": "Keft Û Left (Didinme)",
                  "youtube_url": "https://www.youtube.com/watch?v=P3rRClTXvvg"
          },
          {
                  "name": "Xurşîd",
                  "artist": "Yunus Dişkaya",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xursid.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/xursid.mp3",
                  "album": "Cegerxwîn 1 - Xurşîd",
                  "youtube_url": "https://www.youtube.com/watch?v=8flW1iAwowU"
          },
          {
                  "name": "Yunus Dişkaya - Diqurçimin Gulên Te Pt.2 [Official Audio]",
                  "artist": "Yunus Dişkaya Official",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-diqurcimin-gulen-te-pt-2-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-diqurcimin-gulen-te-pt-2-official-audio.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=OLKbuhRl2kM"
          },
          {
                  "name": "Yunus Dişkaya - Law Hakimo (Official Audio)",
                  "artist": "Red Music Digital",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-law-hakimo-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-law-hakimo-official-audio.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=Algl6vihrUI"
          },
          {
                  "name": "Yunus Dişkaya - Wek Zarê Xwe Pt 1 [Official Audio]",
                  "artist": "Yunus Dişkaya Official",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-wek-zare-xwe-pt-1-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-wek-zare-xwe-pt-1-official-audio.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=DDeCrPA-40U"
          },
          {
                  "name": "Yunus Dişkaya - Wek Zarê Xwe Pt  2  [Official Audio]",
                  "artist": "Red Music Digital",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-wek-zare-xwe-pt-2-official-audio.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/yunus-diskaya-wek-zare-xwe-pt-2-official-audio.mp3",
                  "album": "",
                  "youtube_url": "https://www.youtube.com/watch?v=HpN434x40QE"
          },
          {
                  "name": "Zeviyen Edabe",
                  "artist": "Rewşen",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/zeviyen-edabe.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/zeviyen-edabe.mp3",
                  "album": "Pola Nû",
                  "youtube_url": "https://www.youtube.com/watch?v=zpadF7kELCE"
          },
          {
                  "name": "Zibilldan",
                  "artist": "Nickwan Qaderi",
                  "cover_art_url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/zibilldan.jpeg",
                  "url": "https://gitlab.com/kurdishrock/raw-stream/-/raw/master/stream/zibilldan.mp3",
                  "album": "Single",
                  "youtube_url": "https://www.youtube.com/watch?v=Btt3RWcDgkc"
          }
	]
}
);
