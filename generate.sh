#!/bin/bash
source config.conf

if [ "$#" -lt 1 ]; then
        echo "./`basename $0` slug | cover | playlist | json | html | etherpad | sitemap | download | init_json"
	echo ""
	echo "slug              rename song files as clean url"
	echo "cover             extract song thumb images"
	echo "playlist          download youtube playlist"
	echo "json              print json data of songs"
	echo "html              print html for AmplitudeJS"
	echo "etherpad          get backup of etherpad source"
	echo "download          create download links"
	echo "init_json         print the json data as live"
        exit 1
fi

function slug() {
	cd stream/
	for i in *.mp3; do
		post=$(echo $i | rev | cut -f 2- -d '.' | rev | iconv -t ascii//TRANSLIT | sed -r s/[~\^]+//g | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z)
		echo "rename ${i} to ${post}.mp3"
		mv "${i}" "${post}.mp3" &> /dev/null
	done
	cd ..
}

function cover() {
	cd stream/
	for i in *.mp3; do
		post=${i%.*}
		echo "extract ${i} to ${post}.jpeg"
		#ffmpeg -i "${i}" -vf "crop=w='min(iw\,ih)':h='min(iw\,ih)',scale=500:500,setsar=1" -vframes 1 "${post}.jpeg" &> /dev/null
		ffmpeg -i "${i}" "pre-${post}.jpeg" &> /dev/null
		convert -resize 500x500 -background black -compose Copy -gravity center -extent 500x500 "pre-${post}.jpeg" "${post}.jpeg"
	done
	cd ..
}

function playlist() {
	mv stream/*.jpeg sync/
	mv stream/*.mp3 sync/
	cd stream/
	youtube-dl -i -f bestaudio --retries infinite --socket-timeout 5 --embed-thumbnail --add-metadata --extract-audio --audio-format mp3 --audio-quality 0 -o "%(title)s.%(ext)s" $youtube_playlist_url
        cd ..
	echo "youtube-dl -i -f bestaudio --retries infinite --socket-timeout 5 --embed-thumbnail --add-metadata --extract-audio --audio-format mp3 --audio-quality 0 -o \"%(title)s.%(ext)s\" $youtube_playlist_url" > public/data/command.txt
	diff -q stream/ sync/
}

function init_json() {
	cd stream/
	for song in *.mp3; do
		cover="${song%.*}.jpeg"
		artist=$(exiftool -s -s -s -Artist $song 2> /dev/null)
		title=$(exiftool -s -s -s -Title $song 2> /dev/null)
                album=$(exiftool -s -s -s -Album $song 2> /dev/null)
		youtube_url=$(exiftool -s -s -s -UserDefinedText $song 2> /dev/null | egrep -o 'https?://[^ ]+')
cat <<EOT
          {
                  "name": "${title//\"/\\\"}",
                  "artist": "${artist//\"/\\\"}",
                  "cover_art_url": "${RAW}/${cover}",
                  "url": "${RAW}/${song}",
                  "album": "${album//\"/\\\"}",
                  "youtube_url": "${youtube_url}"
          },
EOT
	done
	cd ..
}

function json() {
cat <<EOT
{
        "songs": [
		$(init_json | sed '$s/,$//')
	]
}
EOT
}

function html() {
	i=0
	cd stream/
	for song in *.mp3; do
                artist=$(exiftool -s -s -s -Artist $song 2> /dev/null)
                title=$(exiftool -s -s -s -Title $song 2> /dev/null)
		duration=$(exiftool -s -s -s -Duration $song 2> /dev/null | head -c 8 | tail -c 5)

cat <<EOT
			<div class="song amplitude-song-container amplitude-play-pause" data-amplitude-song-index="${i}">
				<div class="song-now-playing-icon-container">
					<div class="play-button-container"></div>
					<img class="now-playing" src="./img/now-playing.svg"/>
				</div>
				<div class="song-meta-data">
					<span class="song-title">${title}</span>
					<span class="song-artist">${artist}</span>
				</div>
				<a href="${RAW}/${song}" class="download-link" target="_blank">
					<img class="download-grey" src="./img/download-grey.svg"/>
					<img class="download-white" src="./img/download-white.svg"/>
				</a>
				<span class="song-duration">${duration}</span>
			</div>
EOT

	i=$((i+1))
	done
	cd ..
}

function etherpad() {
	wget -O public/data/etherpad.html ${etherpad_url}/export/html
	wget -O public/data/etherpad.etherpad ${etherpad_url}/export/etherpad
}

function download() {
	cd stream/
	for i in *.mp3; do echo ${RAW}/${i}; done > ../public/data/download.txt
	cd ..
cat > public/data/download.html <<EOF
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>How to download songs?</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/article.css">
	<style>
	pre {
	    white-space: pre-wrap;	 /* Since CSS 2.1 */
	    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
	    white-space: -pre-wrap;	 /* Opera 4-6 */
	    white-space: -o-pre-wrap;    /* Opera 7 */
	    word-wrap: break-word;	 /* Internet Explorer 5.5+ */
	}
	</style>
</head>
<body>
	<h1>How to download songs?</h1>
	<p>Use wget command line tool to download all songs to your local as using *nix terminal:</p>
	<pre>wget -q -O - <a href="download.txt">${site_url}/data/download.txt</a> | wget -i -</pre>
</body>
</html>
EOF
}

#MAIN
$1
