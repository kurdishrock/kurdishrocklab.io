#!/bin/bash
. generate.sh true &>/dev/null #import functions and configs

json > public/data/songs.json

cat > public/js/songs.js <<EOF
Amplitude.init(
$(cat public/data/songs.json)
);
EOF
